<?php
class Controller
{
	/**
	* The start/main/default page.
	*/
	function ShowFirstPage($f3)
	{
		echo \Template::instance () -> render($f3 -> get('VIEW'). 'main.html');
	}

	function ShowClass($f3)
	{
		$class = $f3->get("PARAMS.className");
		$g = new $class();
		$g->name = "Test";
		$g->id = "Test2";

		var_dump($g);
	}

	/**
	* Pre-execution method BEFORE a route call.
	*/
	function beforeroute($f3)
	{
		$currentRoute = $f3->hive()['PARAMS'][0];
		$f3->set('currentRoute', $currentRoute );

		// Include header before route
		echo Template::instance()->render($f3->get('VIEW'). 'defaults/header.html');
	}

	/**
	* Post-execution method AFTER a route call.
	*/
	function afterroute($f3)
	{
		// Include footer after route
		echo Template::instance()->render($f3->get('VIEW'). 'defaults/footer.html');
	}
}
?>
