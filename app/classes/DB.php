<?php
class DB
{

	static $connection;

	private static function ConnectToDB()
	{
		// Try and connect to the database
		self::$connection;

		if(self::$connection === false)
		{
			// Handle error - notify administrator, log to a file, show an error screen, etc.
		}

		echo "PLACEHOLDER: Connecting to DB<br>";
		return true;
	}

	public static function SaveGeneric($object)
	{
		self::CheckAndSetConnection();

		echo "PLACEHOLDER: Writing ".$object->name." to DB<br>";
	}

	private static function CheckAndSetConnection()
	{
		if(isset(self::$connection))
		{
			return true;
		}
		else
		{
			return self::ConnectToDB();
		}
	}


}
?>
