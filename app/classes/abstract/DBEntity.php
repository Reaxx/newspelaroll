<?php
abstract class DBEntity
{
	protected $changed;

	public function __get($var)
	{
		return isset($this->$var) ? $this->$var : false;
	}

	public function __set($var, $value)
	{
		if(!property_exists($this,$var))
		{
			throw new Exception($var." does not exsist in ".get_class($this));
		}
		else
		{
			$this->$var = $value;
			$this->changed = true;
		}
	}

	function __destruct()
	{
		if($this->changed)
		{
			DB::SaveGeneric($this);
		}
	}

}

?>
