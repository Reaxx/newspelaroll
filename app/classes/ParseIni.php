<?php
/**
* ParseIni 1.0
* Parses config-files and saves values to memory or in the f3-hive
*/
class ParseIni
{
	static $ini = null;

	/**
	* SetIni
	* Loads a configfile into self::$ini for easier access
	*
	*@param string $path  path to the configfile.
	*/
	private static function SetIni($path)
	{
		if(!file_exists($path)) { die("Error: Configfile not found");}
		self::$ini = self::GetIni($path);
	}

	/**
	* Get
	* Returns the selected parts of the configfile, returns a single value only if key is given.
	* Otherwise whole category.
	*
	*@return array Kategories and keys from configfile
	*@param string $category Catgory of the config-file to read.
	*@param string $key Specific key to read, if null. (optional)
	*@param string $path  path to the configfile. (Optional)
	*/
	public static function Get($category, $key = null, $path = null)
	{
		if(!isset(self::$ini) || !empty($path)) { self::SetIni($path); }

		if($key)
		{
			return self::$ini[$category][$key];
		}
		else
		{
			return self::$ini[$category];
		}
	}

	/**
	* GetIni
	* Retruns parse_ini_file with preserved bool values on the given path
	*
	*@return array assoc array of the config-file
	*@param string $path Path to configfile. (optional)
	*/
	private static function GetIni($path)
	{
		return parse_ini_file($path, TRUE, INI_SCANNER_TYPED);
	}
}
?>
