<?php

# Kickstart the framework
$f3=require('../lib/base.php');

# Load config
$f3->config('../app/config.ini');

# Define routes
$f3->config('../app/routes.ini');


# Execute the application and start matching routes against
# incoming URIs and call the route handler.
$f3->run();

?>
